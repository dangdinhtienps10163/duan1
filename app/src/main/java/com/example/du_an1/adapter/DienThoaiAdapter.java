package com.example.du_an1.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.du_an1.dao.DienThoai_DAO;
import com.example.du_an1.model.DienThoai;

import java.util.ArrayList;

public class DienThoaiAdapter extends BaseAdapter {
    public Activity context;
    DienThoai_DAO dao;
    ArrayList<DienThoai> list;
    public DienThoaiAdapter(Activity context, ArrayList<DienThoai> list){
        super();
        this.context = context;
        this.list = list;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
