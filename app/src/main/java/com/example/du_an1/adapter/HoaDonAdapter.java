package com.example.du_an1.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.du_an1.dao.HoaDon_DAO;
import com.example.du_an1.model.HoaDon;

import java.util.ArrayList;

public class HoaDonAdapter extends BaseAdapter {
    public Activity context;
    HoaDon_DAO dao;
    ArrayList<HoaDon> list;
    public HoaDonAdapter(Activity context, ArrayList<HoaDon> list){
        super();
        this.context = context;
        this.list = list;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
