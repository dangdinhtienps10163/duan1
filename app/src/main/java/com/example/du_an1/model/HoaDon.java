package com.example.du_an1.model;

import java.util.Date;

public class HoaDon {
    private String MaHD;
    private String NgayXuatHD;
    private String MaDT;

    public String getMaHD() {
        return MaHD;
    }

    public void setMaHD(String maHD) {
        MaHD = maHD;
    }

    public String  getNgayXuatHD() {
        return NgayXuatHD;
    }

    public void setNgayXuatHD(String  ngayXuatHD) {
        NgayXuatHD = ngayXuatHD;
    }
    public String  getMaDT() {
        return MaDT;
    }

    public void setMaDT(String  maDT) {
        MaDT = maDT;
    }

    public HoaDon() {
    }

    public HoaDon(String maHD, String ngayXuatHD, String maDT) {
        MaHD = maHD;
        MaDT = maDT;
        NgayXuatHD = ngayXuatHD;
    }
}
